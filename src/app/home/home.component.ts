import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  office = 'yahoo';
  array = [
    {
      name: 'prem',
      age: 25
    },
    {
      name: 'kumar',
      age: 24
    },
    {
      name: 'prem kumar',
      age: 23
    },
    {
      name: 'kumarprem',
      age: 22
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
